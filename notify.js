(function () {

    let metas = document.querySelectorAll('meta[role="moni"]');
    let params = "";

    if (metas.length > 0) {
        let value = "";

        metas.forEach(meta => {
            value = meta.name + "=" + meta.content;
            value = value.replace("&", "[et]");

            if (params.length === 0) params = "?";
            else params += "&";

            params += value;
        });

    } else {
        let title = location.hostname;
        let body = "Consultation de la page " + document.querySelector('title').textContent;
        title = title.replace("&", "[et]");
        body = body.replace("&", "[et]");
        params = "?moni:title=" + title + "&moni:body=" + body;
    }

    send(params);

    function send(params) {
        let url = "https://mende.alwaysdata.net/moni/api/v1/send" + params;
        let xhttp = new XMLHttpRequest();
        xhttp.open("GET", url, true);
        xhttp.send();
    }
})();